FROM php:7.3-apache

COPY public/ /var/www/html/

ENV PORT 8080

ENTRYPOINT []

CMD sed -i "s/80/$PORT/g" /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf && docker-php-entrypoint apache2-foreground